from odoo import http
from odoo.http import request


class DashboardController(http.Controller):
    @http.route('/exercise/dashboard/', type='http', auth='public', website=True)
    def dashboard(self):
        revenue = request.env['sale.order'].sudo().search([('state', '=', 'done')]).mapped('amount_total')
        top_products = request.env['product.product'].sudo().search([], order='sales_count desc', limit=8)
        sales_trends = request.env['sale.order'].sudo().search([], order='create_date desc', limit=10).mapped('amount_total')
        datas = []
        for product in top_products:
            datas.append({
                'name': product.display_name
            })
        return request.render('exercise11.dashboard_template',
                              {
                                  'revenue': sum(revenue),
                                  'top_products': top_products,
                                  'sales_trends': sales_trends,
                                  'products': datas
                              })
