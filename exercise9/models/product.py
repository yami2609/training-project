from odoo import fields, models


class Product(models.Model):
    _inherit = 'product.template'

    description = fields.Char('Description')

    def action_update_barcode(self):
        products = self.env['product.template'].sudo().search([])
        for item in products:
            item.write({
                'barcode': item.default_code
            })
        return