from odoo import http
from odoo.http import request


class ProductController(http.Controller):
    @http.route('/product/list', type='json', auth='none', method='GET')
    def list_product(self):
        results = []
        products = request.env['product.template'].get_list_product(0)
        for item in products:
            results.append({
                'name': item.display_name,
                'type': item.type,
                'barcode': item.barcode,
                'category': item.categ_id.complete_name,
                'price': item.list_price
            })
        x = ''
        return {'success': True, 'data': results}
