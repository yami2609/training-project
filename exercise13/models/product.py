from odoo import fields, models, api


class Product(models.Model):
    _inherit = 'product.template'

    description = fields.Char('Description')

    @api.model
    def get_list_product(self, id):
        params = []
        if id:
            params.append(('id', int(id)))
        products = self.env['product.template'].sudo().search(params)

        return products
