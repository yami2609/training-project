from odoo import fields, models


class Product(models.Model):
    _inherit = 'product.template'

    description = fields.Char('Description')

    def action_update_barcode(self):
        for item in self:
            item.write({
                'barcode': item.default_code
            })
        return