from odoo import models, fields


class ProductDescriptionWizard(models.TransientModel):
    _name = 'product.description.wizard'

    product_description = fields.Text('Product Description')

    def create_product_description(self):
        active_id = self.env.context.get('active_id')
        product = self.env['product.template'].browse(active_id)
        product.write({
            'description': self.product_description
        })
        return