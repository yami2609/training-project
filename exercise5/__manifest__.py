{
    "name": "Excercise 5",
    "depends": [
        'base', 'sale'
    ],
    "data": [
        "security/ir.model.access.csv",
        "wizard/product_description_wizard.xml",
        "views/product_form_views.xml",  # Views are data too
    ],
    "application": True,
}