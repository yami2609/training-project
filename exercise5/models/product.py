from odoo import fields, models


class Product(models.Model):
    _inherit = 'product.template'

    description = fields.Char('Description')
