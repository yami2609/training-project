{
    "name": "Excercise 3",
    "depends": [
        'base', 'sale'
    ],
    "data": [
        "views/product_detail_report_views.xml",
        "report/product_detail_report_templates.xml"
    ],
    "application": True,
}