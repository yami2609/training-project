{
    "name": "Excercise 10",
    "depends": [
        'base', 'sale'
    ],
    "data": [
        "data/scheduled_actions.xml",
        "views/product_form_views.xml",  # Views are data too
    ],
    "application": True,
}