odoo.define('exercise8.CountDownWidget', function(require) {
    'use strict';

    var basicFields = require('web.basic_fields');
    var registry = require('web.field_registry');

    var countDownWidget = basicFields.FieldDate.extend({
        _renderReadonly: function () {
            var self = this;
            self._super();
            var date = self.$el.html().substring(0, 12);
            var deadline = new Date(date);
            var internal = setInterval(function() {
                var now = new Date().getTime();
                var distance = deadline - now;
                var subExpired = 'Expired';
                if(distance > 0) {
                    var days = Math.floor(distance/(1000*60*60*24));
                    var hours = Math.floor(distance%(1000*60*60*24)/(1000*60*60));
                    var minutes = Math.floor(distance%(1000*60*60)/(1000*60));
                    var seconds = Math.floor(distance%(1000*60)/1000);
                    subExpired = days + 'd ' +  hours + 'h ' + minutes + 'm ' + seconds + 's';
                }
                self.$el.html(date + ' (' + subExpired +')')
            }, 1000)
        }
    })

    registry.add('count_down', countDownWidget);
})