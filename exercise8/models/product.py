from odoo import fields, models


class Product(models.Model):
    _inherit = 'product.template'

    description = fields.Char('Description')
    expired_date = fields.Date('Expired')
