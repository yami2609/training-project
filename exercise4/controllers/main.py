from odoo import http
from odoo.http import request


class ProductController(http.Controller):
    @http.route('/product/list_page/', type='http', auth='public', website=True)
    def list_page(self):
        return request.render('exercise4.product_list_page')
